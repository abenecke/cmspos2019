#! /usr/bin/env python

class bcolors:
    WARNING = '\033[91m'
    IMPORTANT = '\033[94m'
    ENDC = '\033[0m'
    INFO = '\033[93m' 



#                              .-') _                                           _  .-')     ('-.     .-') _                            .-') _  
#                             ( OO ) )                                         ( \( -O )   ( OO ).-.(  OO) )                          ( OO ) ) 
#    .-----.  .-'),-----. ,--./ ,--,'    ,------.,-.-')   ,----.    ,--. ,--.   ,------.   / . --. //     '._ ,-.-')  .-'),-----. ,--./ ,--,'  
#   '  .--./ ( OO'  .-.  '|   \ |  |\ ('-| _.---'|  |OO) '  .-./-') |  | |  |   |   /`. '  | \-.  \ |'--...__)|  |OO)( OO'  .-.  '|   \ |  |\  
#   |  |('-. /   |  | |  ||    \|  | )(OO|(_\    |  |  \ |  |_( O- )|  | | .-') |  /  | |.-'-'  |  |'--.  .--'|  |  \/   |  | |  ||    \|  | ) 
#  /_) |OO  )\_) |  |\|  ||  .     |/ /  |  '--. |  |(_/ |  | .--, \|  |_|( OO )|  |_.' | \| |_.'  |   |  |   |  |(_/\_) |  |\|  ||  .     |/  
#  ||  |`-'|   \ |  | |  ||  |\    |  \_)|  .--',|  |_.'(|  | '. (_/|  | | `-' /|  .  '.'  |  .-.  |   |  |  ,|  |_.'  \ |  | |  ||  |\    |   
# (_'  '--'\    `'  '-'  '|  | \   |    \|  |_)(_|  |    |  '--'  |('  '-'(_.-' |  |\  \   |  | |  |   |  | (_|  |      `'  '-'  '|  | \   |   
#    `-----'      `-----' `--'  `--'     `--'    `--'     `------'   `-----'    `--' '--'  `--' `--'   `--'   `--'        `-----' `--'  `--'   



from optparse import OptionParser
parser = OptionParser()

parser.add_option('--files', type='string', action='store',
                  dest='files',
                  help='Input files')

parser.add_option('--outname', type='string', action='store',
                  default='outplots.root',
                  dest='outname',
                  help='Name of output file')

parser.add_option('--verbose', action='store_true',
                  default=False,
                  dest='verbose',
                  help='Print debugging info')

parser.add_option('--maxevents', type='int', action='store',
                  default=-1,
                  dest='maxevents',
                  help='Number of events to run. -1 is all events')

parser.add_option('--maxjets', type='int', action='store',
                  default=999,
                  dest='maxjets',
                  help='Number of jets to plot. To plot all jets, set to a big number like 999')

parser.add_option('--xrootd', type='string', action='store',
                  default='cmsxrootd.fnal.gov',
                  dest='xrootd',
                  help='xrootd redirect string')

(options, args) = parser.parse_args()
argv = []



#              (`\ .-') /`                  .-') _     ('-.          .-')    .-') _                                      
#               `.( OO ),'                 (  OO) )  _(  OO)        ( OO ). (  OO) )                                     
#    ,------.,--./  .--.  ,--.      ,-.-') /     '._(,------.      (_)---\_)/     '._ ,--. ,--.      ,------.   ,------. 
# ('-| _.---'|      |  |  |  |.-')  |  |OO)|'--...__)|  .---'      /    _ | |'--...__)|  | |  |   ('-| _.---'('-| _.---' 
# (OO|(_\    |  |   |  |, |  | OO ) |  |  \'--.  .--'|  |          \  :` `. '--.  .--'|  | | .-') (OO|(_\    (OO|(_\     
# /  |  '--. |  |.'.|  |_)|  |`-' | |  |(_/   |  |  (|  '--.        '..`''.)   |  |   |  |_|( OO )/  |  '--. /  |  '--.  
# \_)|  .--' |         | (|  '---.',|  |_.'   |  |   |  .--'       .-._)   \   |  |   |  | | `-' /\_)|  .--' \_)|  .--'  
#   \|  |_)  |   ,'.   |  |      |(_|  |      |  |   |  `---.      \       /   |  |  ('  '-'(_.-'   \|  |_)    \|  |_)   
#    `--'    '--'   '--'  `------'  `--'      `--'   `------'       `-----'    `--'    `-----'       `--'       `--'     

import ROOT
import sys
import copy
from array import array
from DataFormats.FWLite import Events, Handle


#Getting CHS AK4 jets
AK4CHSjethandle  = Handle ("std::vector<pat::Jet>")
AK4CHSjetlabel = ("slimmedJets")

# TODO: Getting PUPPI AK4 jets
# TODO: Getting CHS AK8 jets
# TODO: Getting PUPPI AK8 jets


#TODO: smear your jets. Be carefull to smear only MC and not data!

#TODO: correct your jets. Be carefull to correxct different for data and MC and CHS and PUPPI.

#   .-')                _   .-')       ('-.                                    .-') _          .-') _                            .-') _   .-')    
#  ( OO ).             ( '.( OO )_   _(  OO)                                  ( OO ) )        (  OO) )                          ( OO ) ) ( OO ).  
# (_)---\_) .-'),-----. ,--.   ,--.)(,------.         ,------.,--. ,--.   ,--./ ,--,'  .-----./     '._ ,-.-')  .-'),-----. ,--./ ,--,' (_)---\_) 
# /    _ | ( OO'  .-.  '|   `.'   |  |  .---'      ('-| _.---'|  | |  |   |   \ |  |\ '  .--./|'--...__)|  |OO)( OO'  .-.  '|   \ |  |\ /    _ |  
# \  :` `. /   |  | |  ||         |  |  |          (OO|(_\    |  | | .-') |    \|  | )|  |('-.'--.  .--'|  |  \/   |  | |  ||    \|  | )\  :` `.  
#  '..`''.)\_) |  |\|  ||  |'.'|  | (|  '--.       /  |  '--. |  |_|( OO )|  .     |//_) |OO  )  |  |   |  |(_/\_) |  |\|  ||  .     |/  '..`''.) 
# .-._)   \  \ |  | |  ||  |   |  |  |  .--'       \_)|  .--' |  | | `-' /|  |\    | ||  |`-'|   |  |  ,|  |_.'  \ |  | |  ||  |\    |  .-._)   \ 
# \       /   `'  '-'  '|  |   |  |  |  `---.        \|  |_) ('  '-'(_.-' |  | \   |(_'  '--'\   |  | (_|  |      `'  '-'  '|  | \   |  \       / 
#  `-----'      `-----' `--'   `--'  `------'         `--'     `-----'    `--'  `--'   `-----'   `--'   `--'        `-----' `--'  `--'   `-----'  





#  ('-. .-.           .-')    .-') _                            _  .-')     ('-.     _   .-')      .-')    
# ( OO )  /          ( OO ). (  OO) )                          ( \( -O )   ( OO ).-.( '.( OO )_   ( OO ).  
# ,--. ,--.  ,-.-') (_)---\_)/     '._  .-'),-----.   ,----.    ,------.   / . --. / ,--.   ,--.)(_)---\_) 
# |  | |  |  |  |OO)/    _ | |'--...__)( OO'  .-.  ' '  .-./-') |   /`. '  | \-.  \  |   `.'   | /    _ |  
# |   .|  |  |  |  \\  :` `. '--.  .--'/   |  | |  | |  |_( O- )|  /  | |.-'-'  |  | |         | \  :` `.  
# |       |  |  |(_/ '..`''.)   |  |   \_) |  |\|  | |  | .--, \|  |_.' | \| |_.'  | |  |'.'|  |  '..`''.) 
# |  .-.  | ,|  |_.'.-._)   \   |  |     \ |  | |  |(|  | '. (_/|  .  '.'  |  .-.  | |  |   |  | .-._)   \ 
# |  | |  |(_|  |   \       /   |  |      `'  '-'  ' |  '--'  | |  |\  \   |  | |  | |  |   |  | \       / 
# `--' `--'  `--'    `-----'    `--'        `-----'   `------'  `--' '--'  `--' `--' `--'   `--'  `-----'  


f = ROOT.TFile(options.outname, "RECREATE")
f.cd()
print bcolors.IMPORTANT + "Creating output file: " + options.outname + bcolors.ENDC


#one example histogram. More to be added ;)
h_ptAK4CHS = ROOT.TH1F("h_ptAK4CHS", "AK4 Jet p_{T};p_{T} (GeV)", 300, 0, 300)

#create tree and read in variables
varTree = ROOT.TTree("varTree", "varTree")

#example variable, more to be added
ak4CHSpt = array('f', [-1.])
varTree.Branch('ak4CHSpt', ak4CHSpt, 'ak4CHSpt/F')


#    ('-.        (`-.      ('-.       .-') _  .-') _                                                _ (`-.  
#  _(  OO)     _(OO  )_  _(  OO)     ( OO ) )(  OO) )                                              ( (OO  ) 
# (,------.,--(_/   ,. \(,------.,--./ ,--,' /     '._        ,--.      .-'),-----.  .-'),-----.  _.`     \ 
#  |  .---'\   \   /(__/ |  .---'|   \ |  |\ |'--...__)       |  |.-') ( OO'  .-.  '( OO'  .-.  '(__...--'' 
#  |  |     \   \ /   /  |  |    |    \|  | )'--.  .--'       |  | OO )/   |  | |  |/   |  | |  | |  /  | | 
# (|  '--.   \   '   /, (|  '--. |  .     |/    |  |          |  |`-' |\_) |  |\|  |\_) |  |\|  | |  |_.' | 
#  |  .--'    \     /__) |  .--' |  |\    |     |  |         (|  '---.'  \ |  | |  |  \ |  | |  | |  .___.' 
#  |  `---.    \   /     |  `---.|  | \   |     |  |          |      |    `'  '-'  '   `'  '-'  ' |  |      
#  `------'     `-'      `------'`--'  `--'     `--'          `------'      `-----'      `-----'  `--'      


#open files
files = []

if options.files is None:
    print bcolors.WARNING + "files option is empty. Please use --files ../data/ttbar.txt" + bcolors.ENDC
    exit()


with open(options.files) as filelist:
    for line in filelist:
        if len(line) <= 2:
            continue
        s = 'root://' + options.xrootd + '/' + line.rstrip()
        files.append( s )
        print 'Added ' + s

# loop over files
nevents = 0
for ifile in files :
    print 'Processing file ' + ifile
    events = Events (ifile)
    if options.maxevents > 0 and nevents > options.maxevents :
        break
# loop over events in this file
    i = 0
    for event in events:
        if options.maxevents > 0 and nevents > options.maxevents :
            break
        i += 1
        nevents += 1

        if i % 1000 == 0 :
            print bcolors.INFO +  '    ---> Event ' + str(i)




#    ('-.    .-. .-')                              ('-.   .-') _            _ (`-.                         .-') _     .-')    
#   ( OO ).-.\  ( OO )                           _(  OO) (  OO) )          ( (OO  )                       (  OO) )   ( OO ).  
#   / . --. /,--. ,--.    .---.             ,--.(,------./     '._        _.`     \ ,--.      .-'),-----. /     '._ (_)---\_) 
#   | \-.  \ |  .'   /   / .  |         .-')| ,| |  .---'|'--...__)      (__...--'' |  |.-') ( OO'  .-.  '|'--...__)/    _ |  
# .-'-'  |  ||      /,  / /|  |        ( OO |(_| |  |    '--.  .--'       |  /  | | |  | OO )/   |  | |  |'--.  .--'\  :` `.  
#  \| |_.'  ||     ' _)/ / |  |_       | `-'|  |(|  '--.    |  |          |  |_.' | |  |`-' |\_) |  |\|  |   |  |    '..`''.) 
#   |  .-.  ||  .   \ /  '-'    |      ,--. |  | |  .--'    |  |          |  .___.'(|  '---.'  \ |  | |  |   |  |   .-._)   \ 
#   |  | |  ||  |\   \`----|  |-'      |  '-'  / |  `---.   |  |          |  |      |      |    `'  '-'  '   |  |   \       / 
#   `--' `--'`--' '--'     `--'         `-----'  `------'   `--'          `--'      `------'      `-----'    `--'    `-----'  

        #read in jet collection (example CHS AK4)
        event.getByLabel (AK4CHSjetlabel, AK4CHSjethandle)

        AK4CHSjets = AK4CHSjethandle.product()
        # loop over jets and fill hists
        ijet = 0
        for jet in AK4CHSjets :
            if ijet >= options.maxjets :
                break

            #TODO: add pT and eta requirements for jets you want to look at

            #TODO: apply jetID to jets

            #TODO: correct your jets and smear them

            h_ptAK4CHS.Fill( jet.pt() )
            varTree.Fill()

            ijet+=1

#                        ('-.   ('-.         .-') _               _ (`-.  
#                      _(  OO) ( OO ).-.    ( OO ) )             ( (OO  ) 
#    .-----. ,--.     (,------./ . --. /,--./ ,--,' ,--. ,--.   _.`     \ 
#   '  .--./ |  |.-')  |  .---'| \-.  \ |   \ |  |\ |  | |  |  (__...--'' 
#   |  |('-. |  | OO ) |  |  .-'-'  |  ||    \|  | )|  | | .-') |  /  | | 
#  /_) |OO  )|  |`-' |(|  '--.\| |_.'  ||  .     |/ |  |_|( OO )|  |_.' | 
#  ||  |`-'|(|  '---.' |  .--' |  .-.  ||  |\    |  |  | | `-' /|  .___.' 
# (_'  '--'\ |      |  |  `---.|  | |  ||  | \   | ('  '-'(_.-' |  |      
#    `-----' `------'  `------'`--' `--'`--'  `--'   `-----'    `--'      

f.cd()
f.Write()
f.Close()
